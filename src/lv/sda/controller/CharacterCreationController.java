package lv.sda.controller;

import lv.sda.Player;
import lv.sda.model.entity.classes.SuperHero;
import lv.sda.model.entity.races.Race;
import lv.sda.model.entity.races.RpCharacter;
import lv.sda.view.NicknameSelectionView;
import lv.sda.view.RaceSelectionView;
import lv.sda.view.SpecializationSelectionView;

public class CharacterCreationController {

    public static void createCharacter() {
        //input from user to controller
        String nickname = NicknameSelectionView.askNickname();
        Race race = RaceSelectionView.askForRace(); //HUMAN ORC
        SuperHero spec = SpecializationSelectionView.askForSpecialization();

        //model update
        RpCharacter character = new RpCharacter();
        character.setNickname(nickname);
        character.setRace(race);
        character.setSpecialization(spec);

        Player.getInstance().setCharacter(character);
        Player.getInstance().getCharacters().add(character);
    }

}
