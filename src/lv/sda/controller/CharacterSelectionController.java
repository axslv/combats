package lv.sda.controller;

import lv.sda.Player;
import lv.sda.model.entity.races.RpCharacter;
import lv.sda.view.CharacterSelectionView;

public class CharacterSelectionController {

    public static void selectCharacter() {
        RpCharacter rpCharacter = CharacterSelectionView.fetchSelectedCharacter();
        Player.getInstance().setCharacter(rpCharacter);
    }
}
