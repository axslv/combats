package lv.sda.controller;

import lv.sda.Player;

import java.util.Scanner;

public class PlayController {

    public static void play() {
        CharacterSelectionController.selectCharacter();
        System.out.println(Player.getInstance().getCharacter().getNickname() + " has entered the game");
        System.out.println("Now game starts");

        Scanner keyboard = new Scanner(System.in);
        boolean exit = false;
        System.out.println("Enter command (quit to exit):");

        while (!exit) {
            String input = keyboard.nextLine();
            if(input != null) {

                switch (input) {
                    case "quit":
                        System.out.println("Exit programm");
                        exit = true;
                        break;
                    case "forward":
                        System.out.println(Player.getInstance().getCharacter().getNickname() + " is going forward");
                        // Insted of this we should Fire event
                        //Upon receiving an event we decide what to do
                        //e.g.: System.out.println or move character's image in graphical environment
                        break;
                    case "back":
                        System.out.println(Player.getInstance().getCharacter().getNickname() + " is going backwards");
                        break;
                    case "jump":
                        System.out.println(Player.getInstance().getCharacter().getNickname() + " is going backwards");
                        break;
                }
            }
        }
        keyboard.close();

    }
}
