package lv.sda.eventbus;

import java.util.Observable;
import java.util.Observer;

//consumer
@Deprecated
public class PlayerNews implements Observer {
    private String news;

    @Override
    public void update(Observable observable, Object o) {
        this.news = o.toString();
    }

    public String getNews() {
        return news;
    }

    public void setNews(String news) {
        this.news = news;
    }


}
