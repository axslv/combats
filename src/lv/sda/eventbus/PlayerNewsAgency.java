package lv.sda.eventbus;

import java.util.Observable;

@Deprecated
public class PlayerNewsAgency extends Observable {
    private String news;

    public void setNews(String news) {
        this.news = news;
        setChanged();
        notifyObservers(news);
    }
}
