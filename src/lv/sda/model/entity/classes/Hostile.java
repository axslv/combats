package lv.sda.model.entity.classes;

public interface Hostile {
    public boolean hostileTo(Object c);
}
