package lv.sda.model.entity.classes;

public class SuperHero {
    private Integer strength;
    private Integer agility;
    private Integer intellect;
    private Integer vitality;

    //all these stats are considered to be %
    private Double criticalStrike = 5d;
    private Double hitRating = 70d;
    private Double defenseRating = 10d;

    public Double getCriticalStrike() {
        return criticalStrike;
    }

    public void setCriticalStrike(Double criticalStrike) {
        this.criticalStrike = criticalStrike;
    }

    public Double getHitRating() {
        return hitRating;
    }

    public void setHitRating(Double hitRating) {
        this.hitRating = hitRating;
    }

    public Double getDefenseRating() {
        return defenseRating;
    }

    public void setDefenseRating(Double defenseRating) {
        this.defenseRating = defenseRating;
    }

    public Integer getStrength() {
        return strength;
    }

    public void setStrength(Integer strength) {
        this.strength = strength;
    }

    public Integer getAgility() {
        return agility;
    }

    public void setAgility(Integer agility) {
        this.agility = agility;
    }

    public Integer getIntellect() {
        return intellect;
    }

    public void setIntellect(Integer intellect) {
        this.intellect = intellect;
    }

    public Integer getVitality() {
        return vitality;
    }

    public void setVitality(Integer vitality) {
        this.vitality = vitality;
    }
}
