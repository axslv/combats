package lv.sda.model.entity.races;

public enum Race {
    HUMAN, ELF, DWARF, ORC
}
