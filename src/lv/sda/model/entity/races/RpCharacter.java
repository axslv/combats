package lv.sda.model.entity.races;

import lv.sda.model.entity.classes.Hostile;
import lv.sda.model.entity.classes.SuperHero;

import java.util.Observable;
import java.util.Observer;

/*
This class represent a character.
 */

public class RpCharacter implements Hostile, Observer {

    private String nickname;
    private Race race;
    private Gender gender;
    private SuperHero specialization;

    private Integer hitPoints = 100; //absolute value
    private double coordinate = 0;

    @Override
    public void update(Observable observable, Object o) {
        if (o == "forward") {
            coordinate++;
        } else if (o == "back") {
            coordinate--;
        }
    }


    @Override
    public boolean hostileTo(Object c) {
        return c.getClass().getName() == RpCharacter.class.getName();
    }

    public Integer getHitPoints() {
        return hitPoints;
    }

    public void setHitPoints(Integer hitPoints) {
        this.hitPoints = hitPoints;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Race getRace() {
        return race;
    }

    public void setRace(Race race) {
        this.race = race;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public SuperHero getSpecialization() {
        return specialization;
    }

    public void setSpecialization(SuperHero specialization) {
        this.specialization = specialization;
    }


    public double getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(double coordinate) {
        this.coordinate = coordinate;
    }
}
