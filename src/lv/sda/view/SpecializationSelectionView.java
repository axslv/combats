package lv.sda.view;

import lv.sda.model.entity.classes.Caster;
import lv.sda.model.entity.classes.Healer;
import lv.sda.model.entity.classes.SuperHero;
import lv.sda.model.entity.classes.Warrior;
import lv.sda.util.UserInputService;

import java.util.ArrayList;
import java.util.List;

public class SpecializationSelectionView {

    public static SuperHero askForSpecialization() {
        List<String> choices = new ArrayList<>();
        choices.add("Warrior");
        choices.add("Caster");
        choices.add("Healer");

        switch (UserInputService.getUserInputMultiple("Please, select your specialization now", choices).toString()) {
            case "0":
                return new Warrior();
            case "1":
                return new Caster();
            case "2":
                return new Healer();
            default:
                return new Warrior();
        }
    }
}
